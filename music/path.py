import sys

import bge


def ensure():
    try:
        import scripts
    except ImportError:
        project_root = bge.logic.expandPath('//..')
        sys.path.append(project_root)
        import scripts

    try:
        scripts.is_game
    except AttributeError:
        raise FileNotFoundError("Couldn't find scripts directory")
