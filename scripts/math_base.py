from math import ceil

from mathutils import Vector

XAXIS = Vector((1, 0, 0))
YAXIS = Vector((0, 1, 0))
ZAXIS = Vector((0, 0, 1))


def discretize(value, step_size):
    return ceil(value / step_size) * step_size


def clamp(value, lower, upper):
    return min(max(value, lower), upper)


def lerp(a, b, fac):
    return (b - a) * fac + a
