import bge

from .math_base import XAXIS, YAXIS, ZAXIS


def look_at_char(c):
    camera = c.owner
    char_root = camera.scene.objects['char_root']
    look_vec = (char_root.worldPosition - camera.worldPosition).normalized()
    up_vec = char_root.getAxisVect(ZAXIS)
    camera.alignAxisToVect(up_vec, 1, 0.005)
    camera.alignAxisToVect(-look_vec, 2, 0.03)
