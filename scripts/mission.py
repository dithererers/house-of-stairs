import bge

END_WAIT_TIME = 3


def end_level(c):
    manager = c.owner
    if not manager['end_level']:
        return

    if manager['end_time'] < 0:
        print('ending...')
        manager['end_time'] = bge.logic.getClockTime() + END_WAIT_TIME
        return

    if manager['end_time'] < bge.logic.getClockTime():
        start_next_level(manager.scene)


def start_next_level(scene):
    for ob in scene.objects:
        if 'next_level' in ob:
            print(ob, ob['next_level'])
            bge.logic.startGame('//' + ob['next_level'])
